from __future__ import annotations

import os
import shutil
import unittest

from src.Prefix import Prefix, default_prefix


class PrefixTestCase(unittest.TestCase):
    def setUp(self: PrefixTestCase) -> None:
        self.prefixes = Prefix()
        self.prefixes.set_prefix(23, '1')
        self.prefixes.set_prefix(3, '-')
        self.prefixes.set_prefix(5, '?')

    def tearDown(self: PrefixTestCase) -> None:
        if os.path.exists('data'):
            shutil.rmtree('data')

    def test_pickle(self: PrefixTestCase) -> None:
        prefixes = Prefix()
        data = [(123, '?'),
                (1342, 'd'),
                (1, 'y')]

        for guild_id, prefix in data:
            prefixes.set_prefix(guild_id, prefix)
        del prefixes
        prefixes = Prefix()

        for guild_id, prefix in data:
            self.assertEqual(prefix, prefixes.get_prefix(guild_id))

    def test_get_prefix_non_configured_guild(self: PrefixTestCase) -> None:
        self.assertEqual(default_prefix(), self.prefixes.get_prefix(-1))

    def test_get_prefix(self: PrefixTestCase) -> None:
        self.assertEqual('1', self.prefixes.get_prefix(23))

    def test_set_new_prefix(self: PrefixTestCase) -> None:
        self.prefixes.set_prefix(4, '~')
        self.assertEqual('~', self.prefixes.get_prefix(4))

    def test_set_invalid_prefix(self: PrefixTestCase) -> None:
        with self.assertRaises(ValueError):
            self.prefixes.set_prefix(7, '')
        with self.assertRaises(ValueError):
            self.prefixes.set_prefix(7, '__')

    def test_set_already_configured_prefix(self: PrefixTestCase) -> None:
        self.prefixes.set_prefix(3, '/')
        self.assertEqual('/', self.prefixes.get_prefix(3))

    def test_delete_prefix_for_existent_guild(self: PrefixTestCase) -> None:
        self.prefixes.delete_prefix(5)
        self.assertEqual(default_prefix(), self.prefixes.get_prefix(5))

    def test_delete_prefix_for_non_existent_guild(self: PrefixTestCase) -> None:
        self.prefixes.delete_prefix(6)
        self.assertEqual(default_prefix(), self.prefixes.get_prefix(6))


if __name__ == '__main__':
    unittest.main()
