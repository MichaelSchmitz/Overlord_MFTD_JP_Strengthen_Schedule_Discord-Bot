from __future__ import annotations

import os
import shutil
import unittest

from src.Channels import Channels


class ChannelsTestCase(unittest.TestCase):
    def setUp(self: ChannelsTestCase) -> None:
        self.channels = Channels()
        self.channels.set_channel(23, 32)
        self.channels.set_channel(5, 4321)
        self.channels.set_channel(3, 4321)

    def tearDown(self: ChannelsTestCase) -> None:
        if os.path.exists('data'):
            shutil.rmtree('data')

    def test_pickle(self: ChannelsTestCase) -> None:
        channels = Channels()
        data = [(123, 1),
                (1342, 2),
                (1, 4324)]

        for channel_id, guild_id in data:
            channels.set_channel(guild_id, channel_id)
        del channels
        channels = Channels()

        for channel_id, guild_id in data:
            self.assertEqual(channel_id, channels.get_channel(guild_id))

    def test_get_non_existent_channel(self: ChannelsTestCase) -> None:
        self.assertIsNone(self.channels.get_channel(-1))

    def test_get_channel(self: ChannelsTestCase) -> None:
        self.assertEqual(32, self.channels.get_channel(23))

    def test_set_new_channel(self: ChannelsTestCase) -> None:
        self.channels.set_channel(4, 1234)
        self.assertEqual(1234, self.channels.get_channel(4))

    def test_set_existent_channel(self: ChannelsTestCase) -> None:
        self.channels.set_channel(3, 1234)
        self.assertEqual(1234, self.channels.get_channel(3))

    def test_delete_existent_channel(self: ChannelsTestCase):
        self.channels.delete_channel(5)
        self.assertIsNone(self.channels.get_channel(5))

    def test_delete_non_existent_channel(self: ChannelsTestCase):
        self.channels.delete_channel(6)
        self.assertIsNone(self.channels.get_channel(6))

if __name__ == '__main__':
    unittest.main()
