import unittest

from discord import Colour

from src.RoleKey import RoleKey


class MyTestCase(unittest.TestCase):
    def test_colours(self):
        self.assertEqual(Colour.gold(), RoleKey.GOLD.get_colour())
        self.assertEqual(Colour.darker_grey(), RoleKey.CHAR.get_colour())
        self.assertEqual(Colour.dark_gold(), RoleKey.SKILL.get_colour())
        self.assertEqual(Colour.orange(), RoleKey.RELIC.get_colour())
        self.assertEqual(Colour.purple(), RoleKey.INT.get_colour())
        self.assertEqual(Colour.from_rgb(255, 255, 0), RoleKey.HRT.get_colour())
        self.assertEqual(Colour.red(), RoleKey.PWR.get_colour())
        self.assertEqual(Colour.blue(), RoleKey.TEC.get_colour())
        self.assertEqual(Colour.green(), RoleKey.SPD.get_colour())
        with self.assertRaises(ValueError):
            RoleKey.get_colour(None)


if __name__ == '__main__':
    unittest.main()
