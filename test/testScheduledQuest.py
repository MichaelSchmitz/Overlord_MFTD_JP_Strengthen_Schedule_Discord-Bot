from __future__ import annotations

import unittest
from datetime import date

from src.RoleKey import RoleKey
from src.ScheduledQuest import ScheduledQuest

data = [([RoleKey.CHAR, RoleKey.SKILL, RoleKey.RELIC, RoleKey.INT, RoleKey.HRT, RoleKey.PWR], date(2021, 2, 21)),
        ([RoleKey.RELIC, RoleKey.GOLD, RoleKey.CHAR, RoleKey.HRT, RoleKey.PWR, RoleKey.TEC], date(2021, 2, 23)),
        ([RoleKey.CHAR, RoleKey.SKILL, RoleKey.RELIC, RoleKey.PWR, RoleKey.TEC, RoleKey.SPD], date(2021, 2, 25)),
        ([RoleKey.GOLD, RoleKey.CHAR, RoleKey.SKILL, RoleKey.INT, RoleKey.HRT, RoleKey.PWR], date(2021, 2, 26))]


class ScheduledQuestTest(unittest.TestCase):
    def setUp(self: ScheduledQuestTest) -> None:
        self.test_instance = ScheduledQuest()

    def test_current_quest_is_now(self: ScheduledQuestTest) -> None:
        expected = self.test_instance.get_quest_for_date(date.today(), 0, False)
        actual = self.test_instance.get_current_quest(0, False)

        self.assertEqual(expected, actual)

    def test_day_offset(self: ScheduledQuestTest) -> None:
        self.assertEqual(1, ScheduledQuest.calculate_offset_for_day(0, False))
        self.assertEqual(2, ScheduledQuest.calculate_offset_for_day(1, False))
        self.assertEqual(3, ScheduledQuest.calculate_offset_for_day(2, False))
        self.assertEqual(1, ScheduledQuest.calculate_offset_for_day(0, True))
        self.assertEqual(2, ScheduledQuest.calculate_offset_for_day(1, True))
        self.assertEqual(3, ScheduledQuest.calculate_offset_for_day(2, True))

    def test_day_offset_invalid(self: ScheduledQuestTest) -> None:
        with self.assertRaises(ValueError):
            ScheduledQuest.calculate_offset_for_day(3, True)
        with self.assertRaises(ValueError):
            ScheduledQuest.calculate_offset_for_day(3, False)

    def test_correct_key_for_day_samples(self: ScheduledQuestTest) -> None:
        for keys, date in data:
            for index, key in enumerate(keys):
                with self.subTest(date):
                    self.assertEqual(key, self.test_instance.get_quest_for_date(date, index % 3, index >= 3))

if __name__ == '__main__':
    unittest.main()
