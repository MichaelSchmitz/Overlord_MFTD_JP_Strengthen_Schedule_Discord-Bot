from __future__ import annotations

import os
import shutil
import unittest

from src.RoleKey import RoleKey
from src.Roles import Roles, default_roles


class RolesTest(unittest.TestCase):
    def setUp(self: RolesTest) -> None:
        self.roles = Roles()
        self.roles.set_role(23, RoleKey.TEC, '1')
        self.roles.set_role(23, RoleKey.SKILL, 'C1')
        self.roles.set_role(5, RoleKey.INT, 'int')
        self.roles.set_role(3, RoleKey.CHAR, 'char')
        self.roles.set_role(6, RoleKey.GOLD, 'gold')
        self.roles.set_role(6, RoleKey.SPD, 'spd')

    def tearDown(self: RolesTest) -> None:
        if os.path.exists('data'):
            shutil.rmtree('data')

    def test_pickle(self: RolesTest) -> None:
        roles = Roles()
        data = [(123, RoleKey.TEC, 'Tec'),
                (123, RoleKey.INT, 'test'),
                (1, RoleKey.RELIC, '1234')]

        for guild_id, key, name in data:
            roles.set_role(guild_id, key, name)
        del roles
        roles = Roles()

        for guild_id, key, name in data:
            self.assertEqual(name, roles.get_role(guild_id, key))

    def test_get_roles_non_configured_guild(self: RolesTest) -> None:
        self.assertEqual(default_roles(), self.roles.get_roles(-1))

    def test_get_role(self: RolesTest) -> None:
        self.assertEqual('1', self.roles.get_role(23, RoleKey.TEC))
        self.assertEqual('C1', self.roles.get_role(23, RoleKey.SKILL))
        self.assertEqual(default_roles().get(RoleKey.INT), self.roles.get_role(23, RoleKey.INT))

    def test_set_new_role(self: RolesTest) -> None:
        self.roles.set_role(4, RoleKey.PWR, 'pwr')
        self.assertEqual('pwr', self.roles.get_role(4, RoleKey.PWR))

    def test_set_already_configured_role(self: RolesTest) -> None:
        self.roles.set_role(3, RoleKey.CHAR, 'char2')
        self.assertEqual('char2', self.roles.get_role(3, RoleKey.CHAR))

    def test_reset_role_for_existent_guild(self: RolesTest) -> None:
        self.roles.reset_role(6, RoleKey.SPD)
        self.assertEqual('gold', self.roles.get_role(6, RoleKey.GOLD))
        self.assertEqual(default_roles().get(RoleKey.SPD), self.roles.get_role(6, RoleKey.SPD))

    def test_reset_role_for_non_existent_guild(self: RolesTest) -> None:
        self.roles.reset_role(7, RoleKey.SPD)
        self.assertEqual(default_roles(), self.roles.get_roles(7))

    def test_delete_roles_for_existent_guild(self: RolesTest) -> None:
        self.roles.delete_guild(5)
        self.assertEqual(default_roles(), self.roles.get_roles(5))

    def test_delete_roles_for_non_existent_guild(self: RolesTest) -> None:
        self.roles.delete_guild(8)
        self.assertEqual(default_roles(), self.roles.get_roles(8))


if __name__ == '__main__':
    unittest.main()
