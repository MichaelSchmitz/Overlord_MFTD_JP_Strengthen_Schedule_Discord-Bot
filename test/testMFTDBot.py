from __future__ import annotations

import unittest
from datetime import datetime
from typing import Callable, Dict, Coroutine

from unittest import mock
from unittest.mock import AsyncMock, MagicMock, call

import asyncio
from aiohttp import ClientResponse
from discord import Permissions, Colour, Role, Message, Forbidden, Guild, RawReactionActionEvent, Member
from discord.abc import Messageable
from discord.ext.commands import MissingPermissions, CommandNotFound, Context, Bot, CommandError
from discord.ext.tasks import Loop

import src.MFTDBot as MFTDBot
from src.Channels import Channels
from src.Prefix import default_prefix, Prefix
from src.RoleKey import RoleKey
from src.Roles import default_roles, Roles
from src.ScheduledQuest import ScheduledQuest


class MFTDBotTestCase(unittest.TestCase):
    def setUp(self: MFTDBotTestCase) -> None:
        self.loop = asyncio.get_event_loop()

    @mock.patch('src.MFTDBot.Prefix')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_get_prefix_no_guild(self: MFTDBotTestCase, mock_message: Message, mock_prefixes: Prefix) -> None:
        MFTDBot.prefixes = mock_prefixes
        mock_prefixes.get_prefix.return_value = default_prefix()
        mock_message.guild = None
        prefix = self.loop.run_until_complete(MFTDBot.get_guild_prefix(None, mock_message))
        self.assertEqual(default_prefix(), prefix)

    @mock.patch('src.MFTDBot.Prefix')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_get_prefix_custom(self: MFTDBotTestCase, mock_message: Message, mock_prefixes: Prefix) -> None:
        MFTDBot.prefixes = mock_prefixes
        mock_message.guild.id = 1234
        mock_prefixes.get_prefix.return_value = '?'

        prefix = self.loop.run_until_complete(MFTDBot.get_guild_prefix(None, mock_message))
        mock_prefixes.get_prefix.assert_called_once_with(1234)
        self.assertEqual('?', prefix)

    @mock.patch('src.MFTDBot.Prefix')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_get_prefix_default(self: MFTDBotTestCase, mock_message: Message, mock_prefixes: Prefix) -> None:
        MFTDBot.prefixes = mock_prefixes
        mock_prefixes.get_prefix.return_value = default_prefix()
        mock_message.guild.id = 123
        prefix = self.loop.run_until_complete(MFTDBot.get_guild_prefix(None, mock_message))
        self.assertEqual(default_prefix(), prefix)

    @mock.patch('src.MFTDBot.Prefix')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_bot_prefix_set(self: MFTDBotTestCase, mock_message: Message, mock_prefixes: Prefix) -> None:
        MFTDBot.prefixes = mock_prefixes
        mock_message.guild.id = 3435
        mock_prefixes.get_prefix.return_value = '~'
        self.assertEqual('~', self.loop.run_until_complete(MFTDBot.bot.get_prefix(mock_message)))
        mock_prefixes.get_prefix.assert_called_once_with(3435)

    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_global_check_guild_only_dm(self: MFTDBotTestCase, mock_ctx: Context) -> None:
        mock_ctx.guild = None
        result = self.loop.run_until_complete(MFTDBot.global_guild_only(mock_ctx))
        self.assertFalse(result)

    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_global_check_guild_only_guild_message(self: MFTDBotTestCase, mock_ctx: Context) -> None:
        mock_ctx.guild.id = 123
        result = self.loop.run_until_complete(MFTDBot.global_guild_only(mock_ctx))
        self.assertTrue(result)

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_set_prefix_invalid(self: MFTDBotTestCase, mock_message: Message) -> None:
        mock_message.guild.id = 123

        self.loop.run_until_complete(MFTDBot.set_custom_prefix(mock_message, '?!'))

        mock_message.channel.send.assert_awaited_once_with('Invalid prefix, only one character is allowed')

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Prefix')
    def test_set_prefix_valid(self: MFTDBotTestCase, mock_prefixes: MagicMock, mock_message: Message) -> None:
        MFTDBot.prefixes = mock_prefixes
        mock_message.guild.id = 123

        self.loop.run_until_complete(MFTDBot.set_custom_prefix(mock_message, '?'))

        mock_message.channel.send.assert_awaited_once_with('Prefix ? set')
        mock_prefixes.set_prefix.assert_called_once_with(123, '?')

    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_set_prefix_is_guarded(self: MFTDBotTestCase, mock_ctx: Context) -> None:
        self.__is_guarded(mock_ctx, MFTDBot.set_custom_prefix.checks[0])

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_set_custom_role_invalid_role(self: MFTDBotTestCase, mock_message: Message) -> None:
        self.loop.run_until_complete(MFTDBot.set_custom_role(mock_message, 'TEST', 'new'))

        mock_message.channel.send.assert_awaited_once_with('Invalid RoleKey')

    @mock.patch('src.MFTDBot.roles')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_set_custom_role(self: MFTDBotTestCase, mock_message: Message, mock_roles: Roles) -> None:
        mock_message.guild.id = 123

        self.loop.run_until_complete(MFTDBot.set_custom_role(mock_message, 'INT', 'newInt'))

        mock_message.channel.send.assert_awaited_once_with('Role for Key INT updated')
        mock_roles.set_role.assert_called_once_with(123, RoleKey.INT, 'newInt')

    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_set_custom_role_is_guarded(self: MFTDBotTestCase, mock_ctx: Context) -> None:
        self.__is_guarded(mock_ctx, MFTDBot.set_custom_role.checks[0])

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_reset_custom_role_invalid_role(self: MFTDBotTestCase, mock_message: Message) -> None:
        self.loop.run_until_complete(MFTDBot.reset_custom_role(mock_message, 'TEST'))

        mock_message.channel.send.assert_awaited_once_with('Invalid RoleKey')

    @mock.patch('src.MFTDBot.roles')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_reset_custom_role(self: MFTDBotTestCase, mock_message: Message, mock_roles: Roles) -> None:
        mock_message.guild.id = 123

        self.loop.run_until_complete(MFTDBot.reset_custom_role(mock_message, 'GOLD'))

        mock_message.channel.send.assert_awaited_once_with('Role for Key GOLD was reset')
        mock_roles.reset_role.assert_called_once_with(123, RoleKey.GOLD)

    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_reset_custom_role_is_guarded(self: MFTDBotTestCase, mock_ctx: Context) -> None:
        self.__is_guarded(mock_ctx, MFTDBot.reset_custom_role.checks[0])

    @mock.patch('src.MFTDBot.roles')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_list_custom_roles_modified_roles(self: MFTDBotTestCase, mock_message: Message, mock_roles: Roles) -> None:
        mock_message.guild.id = 345
        mock_roles.get_roles.return_value = {RoleKey.SKILL: 'Role1', RoleKey.GOLD: 'Role2', RoleKey.HRT: 'Role 3!'}

        self.loop.run_until_complete(MFTDBot.list_custom_roles(mock_message))

        mock_message.channel.send.assert_awaited_once_with('Roles used:\nSKILL uses role: Role1\n'
                                                           'GOLD uses role: Role2\nHRT uses role: Role 3!\n')

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_list_custom_roles_default(self: MFTDBotTestCase, mock_message: Message) -> None:
        mock_message.guild.id = 123

        self.loop.run_until_complete(MFTDBot.list_custom_roles(mock_message))

        mock_message.channel.send.assert_awaited_once_with('''Roles used:
GOLD uses role: Reminder Gold 金
CHAR uses role: Reminder Character キャラ
SKILL uses role: Reminder Skill スキル
RELIC uses role: Reminder Relic 遺物
INT uses role: Reminder Intelligence 知
HRT uses role: Reminder Heart 心
PWR uses role: Reminder Power 力
TEC uses role: Reminder Technique 技
SPD uses role: Reminder Speed 速\n''')

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_create_all_custom_roles(self: MFTDBotTestCase, mock_message: Message) -> None:
        mock_message.guild.id = 123

        self.loop.run_until_complete(MFTDBot.create_custom_roles(mock_message))

        calls = [
            call(name='Reminder Gold 金', colour=Colour.gold(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Character キャラ', colour=Colour.darker_grey(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Skill スキル', colour=Colour.dark_gold(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Relic 遺物', colour=Colour.orange(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Intelligence 知', colour=Colour.purple(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Heart 心', colour=Colour.from_rgb(255, 255, 0),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Power 力', colour=Colour.red(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Technique 技', colour=Colour.blue(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Speed 速', colour=Colour.green(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
        ]
        mock_message.guild.create_role.assert_has_calls(calls)
        mock_message.channel.send.assert_awaited_once_with('Roles created')

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_create_custom_roles_partly_existent(self: MFTDBotTestCase, mock_message: Message) -> None:
        mock_message.guild.id = 123
        mock_roles = []
        mocked_names = ['Reminder Skill スキル', 'Reminder Speed 速', 'Reminder Gold 金', 'Reminder Heart 心']
        for i in range(4):
            mock_role = AsyncMock(Role)
            mock_role.name = mocked_names[i]
            mock_roles.append(mock_role)
        mock_message.guild.roles = mock_roles

        self.loop.run_until_complete(MFTDBot.create_custom_roles(mock_message))

        calls = [
            call(name='Reminder Character キャラ', colour=Colour.darker_grey(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Relic 遺物', colour=Colour.orange(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Intelligence 知', colour=Colour.purple(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Power 力', colour=Colour.red(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
            call(name='Reminder Technique 技', colour=Colour.blue(),
                 mentionable=True, reason='Created for MFTD JP Str Schedule bot'),
        ]
        mock_message.guild.create_role.assert_has_calls(calls)
        mock_message.channel.send.assert_awaited_once_with('Roles created')

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_create_all_custom_roles_forbidden(self: MFTDBotTestCase, mock_message: Message) -> None:
        mock_message.guild.id = 123
        mock_message.guild.create_role = AsyncMock(side_effect=Forbidden(message='Test',
                                                                         response=MagicMock(ClientResponse)))

        self.loop.run_until_complete(MFTDBot.create_custom_roles(mock_message))
        mock_message.guild.create_role.assert_awaited_once()
        mock_message.channel.send.assert_awaited_once_with('The necessary rights are missing')

    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_create_custom_roles_is_guarded(self: MFTDBotTestCase, mock_ctx: Context) -> None:
        self.__is_guarded(mock_ctx, MFTDBot.create_custom_roles.checks[0])

    @mock.patch('src.MFTDBot.channels')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_use_channel(self: MFTDBotTestCase, mock_message: Message, mock_channels: Channels) -> None:
        mock_message.guild.id = 123
        mock_message.channel.id = 789

        self.loop.run_until_complete(MFTDBot.use_channel(mock_message))

        mock_message.channel.send.assert_awaited_once_with('Channel set')
        mock_channels.set_channel.assert_called_once_with(123, 789)

    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_use_channel_is_guarded(self: MFTDBotTestCase, mock_ctx: Context) -> None:
        self.__is_guarded(mock_ctx, MFTDBot.use_channel.checks[0])

    @mock.patch('src.MFTDBot.roles')
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_reaction_message_modified_roles(self: MFTDBotTestCase, mock_message: Message, mock_roles: Roles) -> None:
        response = '''Select for which quests you want to be notified:
0\uFE0F\u20E3: Reminder Gold 金\n
1\uFE0F\u20E3: test2\n
2\uFE0F\u20E3: Reminder Skill スキル\n
3\uFE0F\u20E3: Reminder Relic 遺物\n
4\uFE0F\u20E3: Reminder Intelligence 知\n
5\uFE0F\u20E3: test\n
6\uFE0F\u20E3: Reminder Power 力\n
7\uFE0F\u20E3: Reminder Technique 技\n
8\uFE0F\u20E3: Reminder Speed 速\n\n'''
        custom_roles = default_roles()
        custom_roles[RoleKey.CHAR] = 'test2'
        custom_roles[RoleKey.HRT] = 'test'
        mock_roles.get_roles.return_value = custom_roles

        calls = [call(response)]
        for i in range(9):
            calls.append(call().add_reaction(f'{i}\uFE0F\u20E3'))
        mock_message.guild.id = 345

        self.loop.run_until_complete(MFTDBot.reaction_message(mock_message))

        mock_message.channel.send.assert_has_calls(calls)
        mock_roles.get_roles.assert_has_calls([call(345), call(345)])

    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    def test_reaction_message_default(self: MFTDBotTestCase, mock_message: Message) -> None:
        response = '''Select for which quests you want to be notified:
0\uFE0F\u20E3: Reminder Gold 金\n
1\uFE0F\u20E3: Reminder Character キャラ\n
2\uFE0F\u20E3: Reminder Skill スキル\n
3\uFE0F\u20E3: Reminder Relic 遺物\n
4\uFE0F\u20E3: Reminder Intelligence 知\n
5\uFE0F\u20E3: Reminder Heart 心\n
6\uFE0F\u20E3: Reminder Power 力\n
7\uFE0F\u20E3: Reminder Technique 技\n
8\uFE0F\u20E3: Reminder Speed 速\n\n'''
        calls = [call(response)]
        for i in range(9):
            calls.append(call().add_reaction(f'{i}\uFE0F\u20E3'))
        mock_message.guild.id = 123

        self.loop.run_until_complete(MFTDBot.reaction_message(mock_message))

        mock_message.channel.send.assert_has_calls(calls)

    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_reaction_message_is_guarded(self: MFTDBotTestCase, mock_ctx: Context) -> None:
        self.__is_guarded(mock_ctx, MFTDBot.reaction_message.checks[0])

    @mock.patch('src.MFTDBot.bot', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.slot_one')
    @mock.patch('src.MFTDBot.slot_two')
    @mock.patch('src.MFTDBot.slot_three')
    @mock.patch('src.MFTDBot.slot_four')
    @mock.patch('src.MFTDBot.slot_five')
    @mock.patch('src.MFTDBot.slot_six')
    def test_on_ready(self: MFTDBotTestCase, mock_slot_six: Loop, mock_slot_five: Loop, mock_slot_four: Loop,
                      mock_slot_three: Loop, mock_slot_two: Loop, mock_slot_one: Loop, mock_bot: Bot) -> None:
        self.loop.run_until_complete(MFTDBot.on_ready())

        mock_bot.change_presence.assert_awaited_once()
        mock_slot_one.start.assert_called_once()
        mock_slot_two.start.assert_called_once()
        mock_slot_three.start.assert_called_once()
        mock_slot_four.start.assert_called_once()
        mock_slot_five.start.assert_called_once()
        mock_slot_six.start.assert_called_once()

    @mock.patch('src.MFTDBot.prepare_reaction_action')
    @mock.patch('src.MFTDBot.RawReactionActionEvent', new_callable=AsyncMock)
    def test_on_reaction_add(self: MFTDBotTestCase, mock_reaction_event: RawReactionActionEvent, mock_prepare: Coroutine) -> None:
        mock_member = AsyncMock(Member)
        mock_role = AsyncMock(Role)
        mock_prepare.return_value = mock_role, mock_member

        self.loop.run_until_complete(MFTDBot.on_raw_reaction_add(mock_reaction_event))

        mock_member.add_roles.assert_awaited_once_with(mock_role)

    @mock.patch('src.MFTDBot.prepare_reaction_action')
    @mock.patch('src.MFTDBot.RawReactionActionEvent', new_callable=AsyncMock)
    def test_on_reaction_remove(self: MFTDBotTestCase, mock_reaction_event: RawReactionActionEvent, mock_prepare: Coroutine) -> None:
        mock_member = AsyncMock(Member)
        mock_role = AsyncMock(Role)
        mock_prepare.return_value = mock_role, mock_member

        self.loop.run_until_complete(MFTDBot.on_raw_reaction_remove(mock_reaction_event))

        mock_member.remove_roles.assert_awaited_once_with(mock_role)

    @mock.patch('src.MFTDBot.bot', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Guild', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.RawReactionActionEvent', new_callable=AsyncMock)
    def test_prepare_reaction_action_bot_is_reacting(self: MFTDBotTestCase, mock_reaction_event: RawReactionActionEvent,
                                                     mock_guild: Guild, mock_message: Message, mock_bot: Bot) -> None:
        mock_reaction_event.guild_id = 1234
        mock_reaction_event.user_id = 345

        mock_channel = AsyncMock(Messageable)
        mock_channel.fetch_message.return_value = mock_message

        mock_bot.get_guild.return_value = mock_guild
        mock_bot.fetch_channel.return_value = mock_channel
        mock_bot.user.id = 345

        result = self.loop.run_until_complete(MFTDBot.prepare_reaction_action(mock_reaction_event))

        self.assertIsNone(result)

    @mock.patch('src.MFTDBot.bot', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Guild', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.RawReactionActionEvent', new_callable=AsyncMock)
    def test_prepare_reaction_action_bot_is_not_message_author(self: MFTDBotTestCase,
                                                               mock_reaction_event: RawReactionActionEvent,
                                                               mock_guild: Guild, mock_message: Message,
                                                               mock_bot: Bot) -> None:
        mock_reaction_event.guild_id = 1234
        mock_reaction_event.user_id = 789

        mock_channel = AsyncMock(Messageable)
        mock_channel.fetch_message.return_value = mock_message

        mock_message.author.id = 567

        mock_bot.get_guild.return_value = mock_guild
        mock_bot.fetch_channel.return_value  = mock_channel
        mock_bot.user.id = 345

        result = self.loop.run_until_complete(MFTDBot.prepare_reaction_action(mock_reaction_event))

        self.assertIsNone(result)

    @mock.patch('src.MFTDBot.bot', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Guild', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.RawReactionActionEvent', new_callable=AsyncMock)
    def test_prepare_reaction_action_wrong_emoji(self: MFTDBotTestCase, mock_reaction_event: RawReactionActionEvent,
                                                 mock_guild: Guild, mock_message: Message, mock_bot: Bot) -> None:
        mock_reaction_event.guild_id = 1234
        mock_reaction_event.user_id = 789
        mock_reaction_event.emoji.name = 'test'

        mock_channel = AsyncMock(Messageable)
        mock_channel.fetch_message.return_value = mock_message

        mock_message.author.id = 345

        mock_bot.get_guild.return_value = mock_guild
        mock_bot.fetch_channel.return_value  = mock_channel
        mock_bot.user.id = 345

        result = self.loop.run_until_complete(MFTDBot.prepare_reaction_action(mock_reaction_event))

        self.assertIsNone(result)

    @mock.patch('src.MFTDBot.bot', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Guild', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.RawReactionActionEvent', new_callable=AsyncMock)
    def test_prepare_reaction_action_no_rolekey_found(self: MFTDBotTestCase, mock_reaction_event: RawReactionActionEvent,
                                                   mock_guild: Guild, mock_message: Message, mock_bot: Bot) -> None:
        mock_reaction_event.guild_id = 123
        mock_reaction_event.user_id = 789
        mock_reaction_event.emoji.name = '9\uFE0F\u20E3'

        mock_channel = AsyncMock(Messageable)
        mock_channel.fetch_message.return_value = mock_message

        mock_message.author.id = 345

        mock_bot.get_guild.return_value = mock_guild
        mock_bot.fetch_channel.return_value = mock_channel
        mock_bot.user.id = 345

        result = self.loop.run_until_complete(MFTDBot.prepare_reaction_action(mock_reaction_event))

        self.assertIsNone(result)

    @mock.patch('src.MFTDBot.roles')
    @mock.patch('src.MFTDBot.bot', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Message', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.Guild', new_callable=AsyncMock)
    @mock.patch('src.MFTDBot.RawReactionActionEvent', new_callable=AsyncMock)
    def test_prepare_reaction_action_role_found(self: MFTDBotTestCase, mock_reaction_event: RawReactionActionEvent,
                                                mock_guild: Guild, mock_message: Message, mock_bot: Bot, mock_roles: Roles) -> None:
        mock_reaction_event.guild_id = 123
        mock_reaction_event.user_id = 789
        mock_reaction_event.emoji.name = '1\uFE0F\u20E3'

        mock_channel = AsyncMock(Messageable)
        mock_channel.fetch_message.return_value = mock_message

        mock_message.author.id = 345

        mock_roles.get_role.side_effect = get_roles_side_effect
        roles = [MagicMock(Role), MagicMock(Role), MagicMock(Role)]
        roles[0].name = 'Test'
        roles[1].name = '2'
        roles[2].name = '3'

        mock_guild.roles = roles
        mock_guild.fetch_member.side_effect = lambda param: MagicMock(id=param)

        mock_bot.fetch_guild.return_value = mock_guild
        mock_bot.fetch_channel.return_value = mock_channel
        mock_bot.user.id = 345

        result = self.loop.run_until_complete(MFTDBot.prepare_reaction_action(mock_reaction_event))

        self.assertEqual('Test', result[0].name)
        self.assertEqual(789, result[1].id)

    @mock.patch('src.MFTDBot.Prefix')
    @mock.patch('src.MFTDBot.CommandError')
    @mock.patch('src.MFTDBot.Context', new_callable=AsyncMock)
    def test_unknown_command_caught(self: MFTDBotTestCase, mock_context: Context, mock_error: CommandError,
                                    mock_prefixes: Prefix) -> None:
        MFTDBot.prefixes = mock_prefixes
        mock_prefixes.get_prefix.return_value = default_prefix()
        mock_error.__class__ = CommandNotFound

        self.loop.run_until_complete(MFTDBot.on_command_error(mock_context, mock_error))

        mock_context.send.assert_awaited_once_with('Unknown command, type !help for information on this bot')

    @mock.patch('src.MFTDBot.Prefix')
    @mock.patch('src.MFTDBot.roles')
    @mock.patch('src.MFTDBot.Guild', new_callable=AsyncMock)
    def test_guild_removal(self: MFTDBotTestCase, mock_guild: Guild, mock_roles: Roles, mock_prefixes: Prefix) -> None:
        MFTDBot.prefixes = mock_prefixes
        mock_guild.id = 123

        self.loop.run_until_complete(MFTDBot.on_guild_remove(mock_guild))

        mock_prefixes.delete_prefix.assert_called_once_with(123)
        mock_roles.delete_guild.assert_called_once_with(123)

    @mock.patch('src.MFTDBot.bot')
    def test_send_notification_no_guild(self: MFTDBotTestCase, mock_bot: Bot) -> None:
        mock_bot.guilds = []
        self.loop.run_until_complete(MFTDBot.send_notification(0, False))

        mock_bot.get_channel.assert_not_called()\

    @mock.patch('src.MFTDBot.Channels')
    @mock.patch('src.MFTDBot.Roles')
    @mock.patch('src.MFTDBot.ScheduledQuest')
    @mock.patch('src.MFTDBot.bot')
    def test_send_notification_no_channel(self: MFTDBotTestCase, mock_bot: Bot, mock_scheduled_quest: ScheduledQuest,
                               mock_roles: Roles, mock_channels: Channels) -> None:
        mock_guild = self.__create_mock_guild(123, {'Test': 'TestMention', '2': 'Mention2', '3': 'Test !!!'})

        MFTDBot.bot = mock_bot
        mock_bot.guilds = [mock_guild]

        MFTDBot.channels = mock_channels
        mock_channels.get_channel.return_value = 4567

        MFTDBot.scheduled_quest = mock_scheduled_quest
        mock_scheduled_quest.get_current_quest.side_effect = get_quest_key_side_effect

        MFTDBot.roles = mock_roles
        mock_roles.get_role.side_effect = get_roles_side_effect

        mock_bot.get_channel.return_value = None

        self.loop.run_until_complete(MFTDBot.send_notification(0, False))

    @mock.patch('src.MFTDBot.Channels')
    @mock.patch('src.MFTDBot.Roles')
    @mock.patch('src.MFTDBot.ScheduledQuest')
    @mock.patch('src.MFTDBot.bot')
    def test_send_notification(self: MFTDBotTestCase, mock_bot: Bot, mock_scheduled_quest: ScheduledQuest,
                               mock_roles: Roles, mock_channels: Channels) -> None:
        mock_guild = self.__create_mock_guild(123, {'Test': 'TestMention', '2': 'Mention2', '3': 'Test !!!'})
        mock_guild_two = self.__create_mock_guild(345, {'role1': 'Role1', 'role2': 'Role2', 'role3': 'Role3'})

        MFTDBot.bot = mock_bot
        mock_bot.guilds = [mock_guild, mock_guild_two]

        MFTDBot.channels = mock_channels
        mock_channels.get_channel.return_value = 4567

        MFTDBot.scheduled_quest = mock_scheduled_quest
        mock_scheduled_quest.get_current_quest.side_effect = get_quest_key_side_effect

        MFTDBot.roles = mock_roles
        mock_roles.get_role.side_effect = get_roles_side_effect

        mock_channel = MagicMock(Messageable)
        mock_bot.get_channel.return_value = mock_channel

        self.loop.run_until_complete(MFTDBot.send_notification(0, False))
        mock_channel.send.assert_has_calls([call('TestMention is up for the next hour'),
                                            call('Role2 is up for the next hour')])

        mock_channel.send.reset_mock()
        self.loop.run_until_complete(MFTDBot.send_notification(0, True))
        mock_channel.send.assert_has_calls([call('Mention2 is up for the next hour'),
                                            call('Role1 is up for the next hour')])

        mock_channel.send.reset_mock()
        self.loop.run_until_complete(MFTDBot.send_notification(1, True))
        mock_channel.send.assert_has_calls([call('Test !!! is up for the next hour'),
                                            call('Role3 is up for the next hour')])

    @mock.patch('src.MFTDBot.send_notification')
    def test_slot_one(self: MFTDBotTestCase, mock_send_notification: Coroutine) -> None:
        MFTDBot.send_notification = mock_send_notification

        self.__test_slot(MFTDBot.slot_one)

        mock_send_notification.assert_awaited_once_with(0, False)

    @mock.patch('src.MFTDBot.send_notification')
    def test_slot_two(self: MFTDBotTestCase, mock_send_notification: Coroutine) -> None:
        MFTDBot.send_notification = mock_send_notification

        self.__test_slot(MFTDBot.slot_two)

        mock_send_notification.assert_awaited_once_with(0, True)

    @mock.patch('src.MFTDBot.send_notification')
    def test_slot_three(self: MFTDBotTestCase, mock_send_notification: Coroutine) -> None:
        MFTDBot.send_notification = mock_send_notification

        self.__test_slot(MFTDBot.slot_three)

        mock_send_notification.assert_awaited_once_with(1, False)

    @mock.patch('src.MFTDBot.send_notification')
    def test_slot_four(self: MFTDBotTestCase, mock_send_notification: Coroutine) -> None:
        MFTDBot.send_notification = mock_send_notification

        self.__test_slot(MFTDBot.slot_four)

        mock_send_notification.assert_awaited_once_with(1, True)

    @mock.patch('src.MFTDBot.send_notification')
    def test_slot_five(self: MFTDBotTestCase, mock_send_notification: Coroutine) -> None:
        MFTDBot.send_notification = mock_send_notification

        self.__test_slot(MFTDBot.slot_five)

        mock_send_notification.assert_awaited_once_with(2, False)

    @mock.patch('src.MFTDBot.send_notification')
    def test_slot_six(self: MFTDBotTestCase, mock_send_notification: Coroutine) -> None:
        MFTDBot.send_notification = mock_send_notification

        self.__test_slot(MFTDBot.slot_six)

        mock_send_notification.assert_awaited_once_with(2, True)

    @mock.patch('src.MFTDBot.before_slot')
    def test_before_slot_one(self: MFTDBotTestCase, mock_before_slot: Coroutine) -> None:
        MFTDBot.before_slot = mock_before_slot

        self.loop.run_until_complete(MFTDBot.before_slot_one())

        mock_before_slot.assert_awaited_once_with(1)

    @mock.patch('src.MFTDBot.before_slot')
    def test_before_slot_two(self: MFTDBotTestCase, mock_before_slot: Coroutine) -> None:
        MFTDBot.before_slot = mock_before_slot

        self.loop.run_until_complete(MFTDBot.before_slot_two())

        mock_before_slot.assert_awaited_once_with(3)

    @mock.patch('src.MFTDBot.before_slot')
    def test_before_slot_three(self: MFTDBotTestCase, mock_before_slot: Coroutine) -> None:
        MFTDBot.before_slot = mock_before_slot

        self.loop.run_until_complete(MFTDBot.before_slot_three())

        mock_before_slot.assert_awaited_once_with(7)

    @mock.patch('src.MFTDBot.before_slot')
    def test_before_slot_four(self: MFTDBotTestCase, mock_before_slot: Coroutine) -> None:
        MFTDBot.before_slot = mock_before_slot

        self.loop.run_until_complete(MFTDBot.before_slot_four())

        mock_before_slot.assert_awaited_once_with(10)

    @mock.patch('src.MFTDBot.before_slot')
    def test_before_slot_five(self: MFTDBotTestCase, mock_before_slot: Coroutine) -> None:
        MFTDBot.before_slot = mock_before_slot

        self.loop.run_until_complete(MFTDBot.before_slot_five())

        mock_before_slot.assert_awaited_once_with(11)

    @mock.patch('src.MFTDBot.before_slot')
    def test_before_slot_six(self: MFTDBotTestCase, mock_before_slot: Coroutine) -> None:
        MFTDBot.before_slot = mock_before_slot

        self.loop.run_until_complete(MFTDBot.before_slot_six())

        mock_before_slot.assert_awaited_once_with(14)

    @mock.patch('src.MFTDBot.asyncio')
    @mock.patch('src.MFTDBot.datetime')
    def test_before_slot(self: MFTDBotTestCase, mock_time: datetime, mock_asyncio: Coroutine) -> None:
        global count
        time = datetime.now()
        start = time.hour * 3600 + time.minute * 60 + time.second
        count = start

        mock_time.now.side_effect = get_datetime_based_on_sleep_calls
        mock_asyncio.sleep.side_effect = increment_counter_for_sleep

        wait_till = (time.hour + 3) % 24
        self.loop.run_until_complete(MFTDBot.before_slot(wait_till))
        self.assertIn(count - start, range(2 * 3600 + 1, 3 * 3600))

        wait_till = (time.hour + 15) % 24
        count = start
        self.loop.run_until_complete(MFTDBot.before_slot(wait_till))
        self.assertIn(count - start, range(14 * 3600 + 1, 15 * 3600))

    @classmethod
    def __create_mock_guild(cls: MFTDBotTestCase, guild_id: int, roles: Dict[str, str]) -> Guild:
        guild = MagicMock(Guild)
        guild.id = guild_id
        guild_roles = []
        for name, mention in roles.items():
            role = MagicMock(Role)
            role.name = name
            role.mention = mention
            guild_roles.append(role)
        guild.roles = guild_roles
        return guild

    def __test_slot(self: MFTDBotTestCase, slot: Loop) -> None:
        self.assertEqual(24, slot.hours)
        self.assertEqual(0, slot.minutes)
        self.assertEqual(0, slot.seconds)
        self.assertEqual(Loop, type(slot))
        self.loop.run_until_complete(slot())

    def __is_guarded(self: MFTDBotTestCase, mock_ctx: Context, check: Callable) -> None:
        # has check
        self.assertEqual('has_guild_permissions', check.__qualname__.split('.')[0])

        # missing permission admin
        with self.assertRaises(MissingPermissions):
            check(mock_ctx)

        # permission set
        mock_ctx.author.guild_permissions = Permissions(administrator=True)
        self.assertTrue(check(mock_ctx))


count = 0


def get_datetime_based_on_sleep_calls() -> datetime:
    return datetime(year=2021, month=2, day=10, hour=int(count / 3600) % 24)


async def increment_counter_for_sleep(increment: int) -> None:
    global count
    count += increment


def get_quest_key_side_effect(offset: int, limit_break: bool) -> RoleKey:
    values = {True: {0: RoleKey.HRT, 1: RoleKey.INT},
              False: {0: RoleKey.CHAR}}
    return values[limit_break][offset]


def get_roles_side_effect(guild_id: int, key: RoleKey) -> str:
    values = {123: {RoleKey.CHAR: 'Test', RoleKey.HRT: '2', RoleKey.INT: '3'},
              345: {RoleKey.CHAR: 'role2', RoleKey.HRT: 'role1', RoleKey.INT: 'role3'}}
    return values[guild_id][key]


if __name__ == '__main__':
    unittest.main()
