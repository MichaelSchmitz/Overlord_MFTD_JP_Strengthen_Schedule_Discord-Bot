# Discord Bot for reminders of the Strengthen Quest Schedule in Overlord - Mass for the Dead (MFTD)

Pings configurable roles, when a strengthen quest is held in game.
Roles are assigned to users based on reactions to a sent message.

## Run Docker Image
You need a file called `.env` in the same folder as the Python script in order to authenticate with Discord.

Example content:
```bash
DISCORD_TOKEN=superSecretToken
```

If you want to use the [Docker Image](https://hub.docker.com/repository/docker/bl4d3s/japanese-char-discord-bot), then you can mount the `.env` file (assuming it's located in your current directory) and run the bot with the following command.
Additionally one volume for storing data is mounted.
```bash
docker run -it --name japanese-char-discord-bot -v $pwd/.env:/usr/src/app/.env -v mftd_discord_bot_data:/usr/src/app/data bl4d3s/japanese-char-discord-bot
```

[Invite](https://discord.com/api/oauth2/authorize?client_id=812594626392227860&permissions=268511296&scope=bot) my hosted version of the bot to your server


# Usage

To receive reminders use command `!useChannel` in the channel you wish to receive reminders.

Afterwards use `!reactionMessage` to print a message to which members of the server can react to with the emojis given.

![Message echoed for self assignment](doc/self_assignment_message.png)

Users can now react to this message and are automatically assigned the corresponding roles.


The roles can either be created manually (with the correct name) or can be created by the bot with `!createRoles`
The bot pings each role, when the Strengthen quest starts.

Example:

![Example of pings](doc/reminders.png)

## Further information

All commands are given descriptions in order for the help command to display their usage.
A custom prefix can be set with `!SetPrefix`.

![Help output](doc/help.png)

### Default roles

The default roles are the following:

![Default roles](doc/default_roles.png)