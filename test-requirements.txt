discord.py~=1.6.0
python-dotenv~=0.15.0
asyncio~=3.4.3

# test requirements
coverage~=5.4