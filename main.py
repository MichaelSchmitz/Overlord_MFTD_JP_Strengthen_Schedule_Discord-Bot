import os
import time

from dotenv import load_dotenv

import src.MFTDBot as MFTDBot

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

# ensure timezone is UTC on linux
if hasattr(time, 'tzset'):
    os.environ['TZ'] = 'Etc/UTC'
    time.tzset()

MFTDBot.bot.run(TOKEN)
