from __future__ import annotations

from collections import defaultdict
from typing import Dict

from .Pickable import Pickable
from .RoleKey import RoleKey


def default_roles() -> Dict[RoleKey, str]:
    return {RoleKey.GOLD:  'Reminder Gold 金',
            RoleKey.CHAR:  'Reminder Character キャラ',
            RoleKey.SKILL: 'Reminder Skill スキル',
            RoleKey.RELIC: 'Reminder Relic 遺物',
            RoleKey.INT:   'Reminder Intelligence 知',
            RoleKey.HRT:   'Reminder Heart 心',
            RoleKey.PWR:   'Reminder Power 力',
            RoleKey.TEC:   'Reminder Technique 技',
            RoleKey.SPD:   'Reminder Speed 速'
            }


class Roles(Pickable):
    __roles_per_guild: Dict[int, Dict[RoleKey, str]] = defaultdict(default_roles)

    def __init__(self: Roles) -> None:
        self.__load()

    def set_role(self: Roles, guild_id: int, role_key: RoleKey, role_name: str) -> None:
        """
        Sets role for given guild based on given key

        :param guild_id:  Id of guild the prefix should be set for
        :param role_key:  Key of role to set
        :param role_name: New name of role
        :return:          None
        """
        self.__roles_per_guild[guild_id][role_key] = role_name
        self.__save()

    def get_role(self: Roles, guild_id: int, role_key: RoleKey) -> str:
        """
        Queries the custom role for given guild or default if none is set

        :param guild_id: Id of guild to query role for
        :param role_key: Key of role to query
        :return:         Prefix to be used
        """
        return self.__roles_per_guild[guild_id][role_key]

    def reset_role(self: Roles, guild_id: int, role_key: RoleKey) -> None:
        """
        Resets the custom role for given guild to default

        :param guild_id: Id to reset for
        :param role_key: Key of role to reset
        :return:         None
        """
        self.__roles_per_guild[guild_id][role_key] = self.__roles_per_guild[-1][role_key]
        self.__save()

    def delete_guild(self: Roles, guild_id: int) -> None:
        """
        Deletes the complete customisation for given guild

        :param guild_id: Id to delete for
        :return:         None
        """
        if guild_id in self.__roles_per_guild.keys():
            del self.__roles_per_guild[guild_id]
            self.__save()

    def get_roles(self: Roles, guild_id: int) -> Dict[RoleKey, str]:
        """
        Returns a dict copy of roles for given guild

        :param guild_id: Id to query for
        :return:         Dict[RoleKey, str] containing all roles
        """
        return self.__roles_per_guild[guild_id].copy()

    def __save(self: Roles) -> None:
        self._save('roles_per_guild', self.__roles_per_guild)

    def __load(self: Roles) -> None:
        loaded = self._load('roles_per_guild')
        if loaded is not None:
            self.__roles_per_guild = loaded
