from __future__ import annotations

from typing import Dict, Union

from .Pickable import Pickable


class Channels(Pickable):
    __channel: Dict[int, int] = {}

    def __init__(self: Channels) -> None:
        self.__load()

    def set_channel(self: Channels, guild_id: int, channel_id: int) -> None:
        """
        Sets channel for given guild to given id

        :param guild_id:    Id of guild the channel should be set for
        :param channel_id:  Id of channel
        :return:            None
        """
        self.__channel[guild_id] = channel_id
        self.__save()

    def get_channel(self: Channels, guild_id: int) -> Union[int, None]:
        """
        Gets the channel for given guild, None if not found

        :param guild_id: Id of guild to query channel for
        :return:         Channel for guild
        """
        if guild_id in self.__channel.keys():
            return self.__channel[guild_id]
        else:
            return None

    def delete_channel(self: Channels, guild_id: int) -> None:
        """
        Deletes the channel for given guild

        :param guild_id: Id to delete for
        :return:         None
        """
        if guild_id in self.__channel.keys():
            del self.__channel[guild_id]
            self.__save()

    def __save(self: Channels) -> None:
        self._save('channels', self.__channel)

    def __load(self: Channels) -> None:
        loaded = self._load('channels')
        if loaded is not None:
            self.__channel = loaded
