import asyncio
from datetime import datetime
from typing import Tuple, Union

import discord
from discord import Guild, Message, Forbidden, RawReactionActionEvent, Role, User, Member
from discord.ext import commands, tasks
from discord.ext.commands import Bot, Context, CommandError

from .RoleKey import RoleKey
from .Channels import Channels
from .Prefix import Prefix
from .Roles import Roles
from .ScheduledQuest import ScheduledQuest

prefixes = Prefix()
roles = Roles()
channels = Channels()
scheduled_quest = ScheduledQuest()


async def get_guild_prefix(_bot: Bot, message: Message) -> str:
    """
    Queries the custom prefix for guild

    :param _bot:    Unused
    :param message: Message prefix should be queried for
    :return:        Prefix of guild message belongs to
    """
    if message.guild:
        return prefixes.get_prefix(message.guild.id)
    else:
        return prefixes.get_prefix(-1)


bot = commands.Bot(command_prefix=get_guild_prefix)


@bot.check
async def global_guild_only(ctx: Context) -> bool:
    return ctx.guild is not None


@bot.command(name='SetPrefix',
             aliases=['setprefix'],
             help='Sets custom prefix for commands to this bot')
@commands.has_guild_permissions(administrator=True)
async def set_custom_prefix(message: Message, prefix: str):
    """
    Set custom prefix for guild,
    notifies about success of operation via message to channel command was invoked in

    :param message: Message sent
    :param prefix:  Prefix given as parameter
    :return:        None
    """
    if len(prefix) != 1:
        await message.channel.send('Invalid prefix, only one character is allowed')
    else:
        prefixes.set_prefix(message.guild.id, prefix)
        await message.channel.send(f'Prefix {prefix} set')


@bot.command(name='setRole',
             aliases=['setrole'],
             help=f'Sets custom role for key, key is one of {", ".join([key.name for key in RoleKey])}')
@commands.has_guild_permissions(administrator=True)
async def set_custom_role(message: Message, role_key: str, role_name: str) -> None:
    """
    Sets a custom role name for given key for this guild

    :param message:   Message sent
    :param role_key:  RoleKey given as parameter
    :param role_name: Role name to use
    :return:          None
    """
    try:
        role_key = RoleKey[role_key]
        roles.set_role(message.guild.id, role_key, role_name)
        await message.channel.send(f'Role for Key {role_key.name} updated')
    except KeyError:
        await message.channel.send('Invalid RoleKey')


@bot.command(name='resetRole',
             aliases=['resetrole'],
             help=f'Resets custom role for key, key is one of {", ".join([key.name for key in RoleKey])}')
@commands.has_guild_permissions(administrator=True)
async def reset_custom_role(message: Message, role_key: str) -> None:
    """
    Resets a custom role (with given key) for this guild to the default value

    :param message:  Message sent
    :param role_key: RoleKey given as parameter
    :return:         None
    """
    try:
        role_key = RoleKey[role_key]
        roles.reset_role(message.guild.id, role_key)
        await message.channel.send(f'Role for Key {role_key.name} was reset')
    except KeyError:
        await message.channel.send('Invalid RoleKey')


@bot.command(name='listRoles',
             aliases=['listroles'],
             help=f'Lists all roles for notifications')
async def list_custom_roles(message: Message) -> None:
    """
    Lists all notification roles for this guild

    :param message: Message sent
    :return:        None
    """
    response = 'Roles used:\n'
    for key, value in roles.get_roles(message.guild.id).items():
        response += f'{key.name} uses role: {value}\n'
    await message.channel.send(response)


@bot.command(name='createRoles',
             aliases=['createroles'],
             help=f'Creates all roles for notifications, if not already present')
@commands.has_guild_permissions(administrator=True)
async def create_custom_roles(message: Message) -> None:
    """
    Creates all roles for this guild

    :param message: Message sent
    :return:        None
    """
    guild: Guild = message.guild
    try:
        for role_key, role_name in roles.get_roles(guild.id).items():
            if role_name not in [role.name for role in guild.roles]:
                await guild.create_role(name=role_name,
                                        colour=role_key.get_colour(),
                                        mentionable=True,
                                        reason='Created for MFTD JP Str Schedule bot')
        await message.channel.send('Roles created')
    except Forbidden:
        await message.channel.send('The necessary rights are missing')


@bot.command(name='useChannel',
             aliases=['usechannel'],
             help=f'Use this channel for notifications')
@commands.has_guild_permissions(administrator=True)
async def use_channel(message: Message) -> None:
    """
    Saves this channel for notifications

    :param message: Message sent
    :return:        None
    """
    channels.set_channel(message.guild.id, message.channel.id)
    await message.channel.send('Channel set')


@bot.command(name='reactionMessage',
             aliases=['reactionmesssage'],
             help=f'Responds with reaction message to use as self assign for roles')
@commands.has_guild_permissions(administrator=True)
async def reaction_message(message: Message) -> None:
    """
    Responds with a message and reactions to use as self assignment board for roles

    :param message: Message sent
    :return:        None
    """
    response = 'Select for which quests you want to be notified:\n'
    for index, role in enumerate(roles.get_roles(message.guild.id).items()):
        response += f'{index}\uFE0F\u20E3: {role[1]}\n\n'
    sent_message = await message.channel.send(response)
    for index in range(len(roles.get_roles(message.guild.id))):
        await sent_message.add_reaction(f'{index}\uFE0F\u20E3')


@bot.event
async def on_raw_reaction_add(reaction_event: RawReactionActionEvent):
    """
    If the reaction was added to a message from this bot and
     is one of the number emojis :zero:, :one:, ... :nine:,
     then assign responding user the corresponding role

     Numbers are only valid for the number of roles

    :param reaction_event: RawReactionActionEvent to respond to
    :return:               None
    """
    role_member = await prepare_reaction_action(reaction_event)
    if role_member is not None:
        await role_member[1].add_roles(role_member[0])


@bot.event
async def on_raw_reaction_remove(reaction_event: RawReactionActionEvent):
    """
    If the reaction was removed from a message from this bot and
     is one of the number emojis :zero:, :one:, ... :nine:,
     then remove the corresponding role from responding user

     Numbers are only valid for the number of roles

    :param reaction_event: RawReactionActionEvent to respond to
    :return:               None
    """
    role_member = await prepare_reaction_action(reaction_event)
    if role_member is not None:
        await role_member[1].remove_roles(role_member[0])


async def prepare_reaction_action(reaction_event: RawReactionActionEvent) -> Union[Tuple[Role, Member], None]:
    """
    If the reaction was added/removed to/from a message from this bot and
     is one of the number emojis :zero:, :one:, ... :nine:,
     then return the member and role to add/remove

     Numbers are only valid for the number of roles (currently 0-8)

    :param reaction_event: RawReactionActionEvent to respond to
    :return:               Tuple[Role, Member] or None
    """
    guild = await bot.fetch_guild(reaction_event.guild_id)
    channel = await bot.fetch_channel(reaction_event.channel_id)
    message = await channel.fetch_message(reaction_event.message_id)
    if reaction_event.user_id != bot.user.id and message.author.id == bot.user.id:
        role = None
        if reaction_event.emoji.name.endswith('\uFE0F\u20E3'):
            try:
                role_key = RoleKey(int(reaction_event.emoji.name[0]))
            except ValueError:
                return None
            role = next(
                (role for role in guild.roles if role.name == roles.get_role(reaction_event.guild_id, role_key)), None)
        if role is not None:
            member = await guild.fetch_member(reaction_event.user_id)
            return role, member
    return None


@bot.event
async def on_ready() -> None:
    """
    Change presence and log successful connection

    :return: None
    """
    print(f'{bot.user.name} has connected to Discord!')
    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.playing, name='MFTD JP Reminder'))
    slot_one.start()
    slot_two.start()
    slot_three.start()
    slot_four.start()
    slot_five.start()
    slot_six.start()


@bot.event
async def on_command_error(ctx: Context, error: CommandError) -> None:
    """
    Notify user of unknown command instead of silently logging to stderr (default)

    :param ctx:   Context of CommandError
    :param error: Exception thrown
    :return:      None
    """
    if isinstance(error, discord.ext.commands.errors.CommandNotFound):
        await ctx.send(f'Unknown command, type {prefixes.get_prefix(ctx.guild.id)}help for information on this bot')


@bot.event
async def on_guild_remove(guild: Guild) -> None:
    """
    Deletes all configurations specific to this guild

    :param guild: Guild bot was removed from
    :return:      None
    """
    prefixes.delete_prefix(guild.id)
    roles.delete_guild(guild.id)


async def send_notification(offset: int, limit_break: bool) -> None:
    for guild in bot.guilds:
        channel = bot.get_channel(channels.get_channel(guild.id))
        if channel is None:
            continue
        role = next((role for role in guild.roles if role.name ==
                     roles.get_role(guild.id, scheduled_quest.get_current_quest(offset, limit_break))),
                    None)
        await channel.send(f'{role.mention} is up for the next hour')


@tasks.loop(hours=24)
async def slot_one():
    await send_notification(0, False)


@tasks.loop(hours=24)
async def slot_two():
    await send_notification(0, True)


@tasks.loop(hours=24)
async def slot_three():
    await send_notification(1, False)


@tasks.loop(hours=24)
async def slot_four():
    await send_notification(1, True)


@tasks.loop(hours=24)
async def slot_five():
    await send_notification(2, False)


@tasks.loop(hours=24)
async def slot_six():
    await send_notification(2, True)


@slot_one.before_loop
async def before_slot_one():
    await before_slot(1)


@slot_two.before_loop
async def before_slot_two():
    await before_slot(3)


@slot_three.before_loop
async def before_slot_three():
    await before_slot(7)


@slot_four.before_loop
async def before_slot_four():
    await before_slot(10)


@slot_five.before_loop
async def before_slot_five():
    await before_slot(11)


@slot_six.before_loop
async def before_slot_six():
    await before_slot(14)


async def before_slot(hour: int):
    for _ in range(60 * 60 * 24):  # one day
        if datetime.now().hour == hour:
            return  # hour reached
        await asyncio.sleep(1)  # test every second
