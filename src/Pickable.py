from __future__ import annotations
from typing import Any
import os

import pickle


class Pickable:
    def _save(self: Pickable, name: str, to_save: Any) -> None:
        self.__create_dir_if_not_existent()
        with open(f'data/{name}', 'wb') as f:
            pickle.dump(to_save, f)

    def _load(self: Pickable, name: str) -> Any:
        self.__create_dir_if_not_existent()
        if os.path.exists(f'data/{name}'):
            with open(f'data/{name}', 'rb') as f:
                return pickle.load(f)

    @classmethod
    def __create_dir_if_not_existent(cls: Pickable) -> None:
        if not os.path.exists('data'):
            os.makedirs('data')
