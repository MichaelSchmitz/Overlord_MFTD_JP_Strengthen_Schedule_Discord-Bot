from __future__ import annotations

from collections import defaultdict
from typing import Dict

from .Pickable import Pickable


def default_prefix() -> str:
    return '!'


class Prefix(Pickable):
    __guild_prefixes: Dict[int, str] = defaultdict(default_prefix)

    def __init__(self: Prefix) -> None:
        self.__load()

    def set_prefix(self: Prefix, guild_id: int, prefix: str) -> None:
        """
        Sets prefix for given guild, if prefix is exactly one char long

        :param guild_id: Id of guild the prefix should be set for
        :param prefix:   Prefix to set
        :return:         None
        """
        if len(prefix) != 1:
            raise ValueError('Prefix has to be a single char')
        self.__guild_prefixes[guild_id] = prefix
        self.__save()

    def get_prefix(self: Prefix, guild_id: int) -> str:
        """
        Gets the prefix for given guild or default if none is set

        :param guild_id: Id of guild to query prefix for
        :return:         Prefix to be used
        """
        return self.__guild_prefixes[guild_id]

    def delete_prefix(self: Prefix, guild_id: int) -> None:
        """
        Deletes the custom prefix for given guild

        :param guild_id: Id to delete for
        :return:         None
        """
        if guild_id in self.__guild_prefixes.keys():
            del self.__guild_prefixes[guild_id]
            self.__save()

    def __save(self: Prefix) -> None:
        self._save('guild_prefixes', self.__guild_prefixes)

    def __load(self: Prefix) -> None:
        loaded = self._load('guild_prefixes')
        if loaded is not None:
            self.__guild_prefixes = loaded
