from __future__ import annotations

import math
from datetime import date

from .RoleKey import RoleKey


class ScheduledQuest:
    reference_time: date
    seconds_to_day: int

    def __init__(self: ScheduledQuest) -> None:
        self.reference_time = date(1899, 12, 31)
        self.seconds_to_day = 60 * 60 * 24

    def get_current_quest(self: ScheduledQuest, quest_num: int, limit_break: bool) -> RoleKey:
        """
        Get RoleKey for present day quest with given offset

        :param quest_num:   Number of quest on day (0-2) of type
        :param limit_break: Indicator for type (LimitBreak or LevelUp)
        :return:            RoleKey
        """
        return self.get_quest_for_date(date.today(), quest_num, limit_break)

    def get_quest_for_date(self: ScheduledQuest, timestamp: date, quest_num: int, limit_break: bool) -> RoleKey:
        """
        Get RoleKey for quest on given day with given offset

        :param quest_num:   Number of quest on day (0-2) of type
        :param limit_break: Indicator for type (LimitBreak or LevelUp)
        :return:            RoleKey
        """
        day_offset = ScheduledQuest.calculate_offset_for_day(quest_num, limit_break)
        days_since_1900 = math.floor((timestamp - self.reference_time).total_seconds() / self.seconds_to_day + (32/24))
        remainder = (days_since_1900 * 3 + day_offset) % (4 + limit_break)
        return RoleKey(remainder + 4 if limit_break else remainder)

    @staticmethod
    def calculate_offset_for_day(quest_num: int, limit_break: bool) -> int:
        """
        Calculates the offset depended on quest_number and type of quest

        :param quest_num:   Number of quest on day (0-2) of type
        :param limit_break: Indicator for type (LimitBreak or LevelUp)
        :return:            Offset to be used in calculations
        """
        if quest_num not in range(3):
            raise ValueError('Offset should be between 0 and 2')
        return (quest_num + 1) % (4 + limit_break)
