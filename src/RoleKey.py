from __future__ import annotations

from enum import IntEnum

from discord import Colour


class RoleKey(IntEnum):
    GOLD = 0,
    CHAR = 1,
    SKILL = 2,
    RELIC = 3,
    INT = 4,
    HRT = 5,
    PWR = 6,
    TEC = 7,
    SPD = 8

    def get_colour(self: RoleKey) -> Colour:
        if self is RoleKey.GOLD:
            return Colour.gold()
        elif self is RoleKey.CHAR:
            return Colour.darker_grey()
        elif self is RoleKey.SKILL:
            return Colour.dark_gold()
        elif self is RoleKey.RELIC:
            return Colour.orange()
        elif self is RoleKey.INT:
            return Colour.purple()
        elif self is RoleKey.HRT:
            return Colour.from_rgb(255, 255, 0)
        elif self is RoleKey.PWR:
            return Colour.red()
        elif self is RoleKey.TEC:
            return Colour.blue()
        elif self is RoleKey.SPD:
            return Colour.green()
        else:
            raise ValueError('Invalid enum value')
